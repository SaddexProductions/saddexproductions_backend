const {User, validate} = require('../../../models/user');
const config = require('config');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');

const userObj = {
    _id: new mongoose.Types.ObjectId().toHexString(),
    email: "saddexproductions@gmail.com",
    isAdmin: true
}

const reqObject = {
    email: "saddexproductions@gmail.com",
    password: "testPassword",
    repeatPassword: "testPassword",
    cellphone: "1701234567", //not my real number
    countryCode: "DE"
}

describe("User model", () => {

    it("should return a valid JWT", () => {
        const user = new User(userObj);
        const token = user.generateAuthToken();
        const decoded = jwt.verify(token, config.get("SP_jwtPrivateKey"));
        
        expect(decoded).toMatchObject(userObj);
    });

    it("should contain login property in JWT if generate method is called with login set to true", () => {
        const user = new User(userObj);

        const userObj2 = {...userObj, login: true}
        const token = user.generateAuthToken(true);
        const decoded = jwt.verify(token, config.get("SP_jwtPrivateKey"));

        expect(decoded).toMatchObject(userObj2);
    })

});

describe("User create validation", () => {

    it("should validate correctly when all required details are provided", () => {
        const {error} = validate(reqObject)

        expect(error).toBeFalsy();
    })

    it("should generate an error when a required field is not provided", () => {
        const reqObject2 = {...reqObject};
        delete reqObject2.email;

        const {error} = validate(reqObject2)

        expect(error).toBeTruthy();
    })

    it("should generate an error when email isn't in email format", () => {
        const reqObject2 = {...reqObject, email: "dsgsdgg@"};

        const {error} = validate(reqObject2)

        expect(error).toBeTruthy();
    })

    it("should generate an error when password is too short", () => {
        const reqObject2 = {...reqObject, password: "test"};

        const {error} = validate(reqObject2)

        expect(error).toBeTruthy();
    })

    it("should generate an error when cellphone isn't in cellphone format", () => {
        const reqObject2 = {...reqObject, cellphone: "43gdgsaffs"};
        const {error} = validate(reqObject2)

        expect(error).toBeTruthy();
    })
})
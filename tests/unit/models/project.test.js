const {validate} = require('../../../models/project');

const baseObj = {
    title: "Good project",
    image: "damn.png",
    url: "https://yourmomma.com",
    type: "memes",
    frontEnd: "React",
    backEnd: "NodeJS",
    offset: 0
}

describe("Project model validation", () => {
    
    it("should return an error if one of more required values are missing from request body", () => {
        const res = validate({
            title: "hello"
        });

        expect(res).toHaveProperty("error");
    });

    it("should return an error if length of title is too long", () => {
        const res = validate({
            ...baseObj,
            title: 'x'.repeat(10*1024*1024)
        });

        expect(res.error.details[0].message).toMatch('title" length must be less than or equal to 100 characters long');
    });

    it("should return an error if type isn't a string", () => {
        const res = validate({
            ...baseObj,
            type: 5
        });

        expect(res.error.details[0].message).toMatch('"type" must be a string');
    });

    it("should return an error when the image has no extension", () => {
        const res = validate({
            ...baseObj,
            image: "mychessgame"
        });

        expect(res.error.details[0].message).toMatch(/"image" with value "mychessgame" fails to match the required pattern:/);
    });

    it("should return an error when the image has a non-allowed extension", () => {
        const res = validate({
            ...baseObj,
            image: "mychessgame.psd"
        });

        expect(res.error.details[0].message).toMatch(/"image" with value "mychessgame.psd" fails to match the required pattern:/);
    });
});
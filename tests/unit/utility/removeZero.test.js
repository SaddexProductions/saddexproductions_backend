const removeZero = require("../../../utility/removeZero");

describe("remove zeros from telephone number utility", () => {

    it("should removed the first character in a phone number if it's a zero", () => {

        const result = removeZero("01701234567")

        expect(result.split("")[0]).not.toEqual("0")
    })

    it("should not remove first character if it's not zero", () => {

        const startString = "1701234567";
        const result = removeZero(startString);

        expect(result.length).toEqual(startString.length)
    })
})
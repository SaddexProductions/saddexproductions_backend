const filterUser = require('../../../utility/filterUser');


describe("filter user utility", () => {

    it('filter method should filter details correctly', () => {
        const userToFilter = {
            _id: "334tgdsd",
            email: "saddexproductions@gmail.com",
            password: "examplepassword",
            isAdmin: true,
            twoFactorAuth: {
                authyID: 53535,
                cellphone: 1701234567,
                countryCode: "DE"
            }
        }

        const obj = filterUser(userToFilter);

        expect(obj).toEqual({_id: "334tgdsd", email: "saddexproductions@gmail.com", twoFactorAuth: {cellphone: 1701234567, countryCode: "DE"}})
    })
})
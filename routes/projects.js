//npm modules
const express = require('express');

//local modules
const auth = require('../middleware/auth');
const { Project, validate } = require('../models/project');
const { development } = require('../utility/non-env-constants');
const cors = require('cors');

//setup
const router = express.Router();
if (development) router.use(cors());

router.get('/', async (req, res) => {

    let skip = 0;

    if (req.query.skip) {

        const testSkip = +req.query.skip;
        
        if (typeof testSkip === "number") skip = testSkip;
    }
    
    const results = await Project.find({}, { __v: 0 }).sort({"yMoC.time": -1}).skip(skip).limit(5);
    if (!results || results.length == 0) return res.status(404).send("No projects found");

    res.status(200).json(results);
});

module.exports = router;
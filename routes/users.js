//npm modules
const _ = require('lodash');
const nodemailer = require('nodemailer');
const config = require('config');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const express = require('express');
const hb = require('handlebars');
const fs = require('fs');
const { Client } = require('authy-client');
const { promisify } = require('util');
const cors = require('cors');

//local modules
const { User, validate, validateNewPass, validateEmail } = require('../models/user');
const auth = require('../middleware/auth');
const removeZero = require("../utility/removeZero");
const { development } = require('../utility/non-env-constants');

//setup
const readFileAsync = promisify(fs.readFile);
const router = express.Router();
const key = config.get('authy_key');
const client = new Client({ key });
if (development) router.use(cors());

router.post('/resetpassword', async (req, res) => {
    const { error } = validateEmail(req.body);
    if (error) return res.status(400).send('Validation error: ' + error.details[0].message);

    const email = req.body.email.toLowerCase();

    let user = await User.findOne({ email });
    if (!user) return res.status(404).send("A user with this email was not found.");

    const token = user.generateAuthToken(false);
    try {

        user.validResetToken = token;
        await user.save();
        let transporter = nodemailer.createTransport({
            host: 'smtp.sendgrid.net',
            port: 465,
            secure: true, // true for 465, false for other ports
            auth: {
                user: "apikey",
                pass: config.get('nodemailer_key')
            }
        });
        const html = await readFileAsync('./resources/template.html', { encoding: 'utf-8' });
        let template = hb.compile(html);
        const base = development ? 'http://localhost:3000' : 'https://admin.saddexproductions.com';
        let replacements = {
            url: `${base}/api/users/resetpassword?token=${token}&email=${email}`
        };
        let htmlSend = template(replacements);
        const mailOptions = {
            from: "noreply@admin.saddexproductions.com", // sender address
            to: email, // list of receivers
            subject: "Reset password", // Subject line
            html: htmlSend
        }
        transporter.sendMail(mailOptions, (error, info) => {
            if (err) {
                throw (err);
            }
        });

        res.status(200).send(`A password reset email was sent to the address ${user.email}`);
    }
    catch (ex) {
        return res.status(500).send("Internal server error");
    }
});
router.post('/newpassword', async (req, res) => {
    try {
        const decrypted = jwt.verify(req.header('x-reset-token'), config.get('SP_jwtPrivateKey'));
        if (decrypted.login) return res.status(403).send("This token can't be used for that operation");

        const user = await User.findOne({ email: decrypted.email });
        if (!user) return res.status(404).send("A user with this email was not found.");

        const { error } = validateNewPass(req.body);
        if (error) return res.status(400).send('Validation error: ' + error.details[0].message);

        if (req.body.password != req.body.repeatPassword) return res.status(400).send("The passwords do not match");

        const isSame = await bcrypt.compare(req.body.password, user.password);
        if (isSame) return res.status(400).send("The new password can't be the same as the old password");

        const salt = await bcrypt.genSalt(10);
        user.password = await bcrypt.hash(req.body.password, salt);
        user.validResetToken = "";
        user.passwordUpdatedOn = new Date();
        try {
            await user.save();

            res.status(200).send("Password updated successfully. Redirecting...");
        }
        catch (ex) {
            return res.status(500).send("Something went wrong");
        }
    }
    catch (ex) {
        return res.status(400).send("Invalid token");
    }

});
//Signing up is disabled for this particular site usually
router.post('/', async (req, res) => {

    const { error } = validate(req.body);
    if (error) return res.status(400).send('Validation error: ' + error.details[0].message);

    const email = req.body.email.toLowerCase();

    if (req.body.password !== req.body.repeatPassword) return res.status(400).send('The passwords do not match')

    //loookup user in db

    let user = await User.findOne({ email });
    if (user) return res.status(400).send("A user with this email has already been registered");

    let cellphone;
    let authyID;


    if (!req.body.cellphone || !req.body.countryCode) return res.status(400).send('Telephone or country ISO missing')

    //remove initial zero from cellphone number

    cellphone = removeZero(req.body.cellphone);

    authyID = await addAuthy({
        countryCode: req.body.countryCode,
        email,
        phone: cellphone
    });

    const salt = await bcrypt.genSalt(10);
    const password = await bcrypt.hash(req.body.password, salt);
    user = new User(
        {
            email,
            password,
            migrate: 1,
            twoFactorAuth: {
                cellphone,
                countryCode: req.body.countryCode,
                authyID,
                active: true
            }
        }
    );
    await user.save();
    res.status(200).json(_.pick(user, ['_id', 'email']));
});

router.put('/:id', auth, async (req, res) => {

    const { error } = validateNewPass(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    let user = await User.findById(req.params.id);
    if (!user) return res.status(404).send("User not found");

    if (!req.body.password) return res.status(401).send("Please enter the old password");

    const validPassword = await bcrypt.compare(req.body.password, user.password);
    if (!validPassword) return res.status(400).send('Please enter the current password again, correctly');

    const isSame = await bcrypt.compare(req.body.newPassword, user.password);
    if (isSame) return res.status(400).send("The new password can't be the same as the old password");

    if (req.body.newPassword != req.body.repeatPassword) return res.status(400).send("The passwords do not match");

    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(req.body.newPassword, salt);
    user.passwordUpdatedOn = new Date();
    const token = user.generateAuthToken(true);
    await user.save();

    res.status(200).json({ msg: "Updated password successfully", data: _.pick(user, ['_id', 'email']), token: token });

});
router.get('/resetpassword', async (req, res) => {
    const user = await User.findOne({ validResetToken: req.query.token });
    if (!user) return res.status(400).send("<script type='text/javascript'>(function(){window.location.href = 'http://localhost:3001/login';})()</script>"); //production "../../../login"

    try {
        const decrypted = jwt.verify(req.query.token, config.get('SP_jwtPrivateKey'));
        if (decrypted.email = !req.query.email || decrypted.login) return res.status(403).send("<script type='text/javascript'>(function(){window.location.href = 'http://localhost:3001/login';})()</script>");
    }
    catch (ex) {
        return res.status(400).send("Invalid token");
    }
    const token = user.generateAuthToken(false);

    res.status(200).send("<script type='text/javascript'>(function(){var d = new Date();d.setTime(d.getTime() + (60 * 60 * 1000));var expires = 'expires=' + d.toUTCString();" +
        "document.cookie = 'reset = " + token + ";' + expires + ';path=/';;window.location.href = 'http://localhost:3001/login#reset';})()</script>"); //production "../../../resetpassword"
});

//add authy user, returns Authy id on success
const addAuthy = userData => {
    return new Promise(async (res, rej) => {
        try {
            const { user: { id: authyId } } = await client.registerUser(userData);
            return res(authyId);
        }
        catch(ex) {
            return rej(ex)
        }
    });
}

module.exports = router;
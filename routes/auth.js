//npm modules
const bcrypt = require('bcrypt');
const { Client } = require('authy-client');
const Joi = require('@hapi/joi');
const express = require('express');
const config = require('config');
const jwt = require('jsonwebtoken');
const cors = require('cors');

//local modules
const {User} = require('../models/user');
const auth = require('../middleware/auth');
const trusted = require('../middleware/trusted');
const filterUser = require('../utility/filterUser');
const { development } = require('../utility/non-env-constants');

//setup
const router = express.Router();
const key = config.get('authy_key');
const client = new Client({ key });
if (development) router.use(cors());

router.get('/', auth, async (req, res) => {
    if(req.user.forTwoFactor) return res.status(400).send("Token can't be used for signing in");

    const user = await User.findById(req.user._id);
    if(!user) return res.status(404).send("User not found");

    res.status(200).json(filterUser(user));
});

router.post('/', trusted, async (req, res) => {
    const { error } = validate(req.body); 
    if (error) return res.status(400).send('Validation error: ' + error.details[0].message);

    let user = await User.findOne({ email: req.body.email.toLowerCase() });
    if(!user) return res.status(400).send('Invalid email or password');

    const validPassword = await bcrypt.compare(req.body.password, user.password);
    if(!validPassword) return res.status(400).send('Invalid email or password');

    if(req.bypass){
        const token = user.generateAuthToken(true);
        return res.status(200).json({
            jwt: token, 
            auth: true,
            user: filterUser(user)
        });
    }
    else {
        const token = user.generateAuthToken(true, true);
        res.status(200).json({
            jwt: token,
            twoFactorAuth: true
        });
    }
});

router.get('/requestsms', auth, async (req,res) => {
    if(!req.user.forTwoFactor) return res.status(400).send("This token can't be used for two-factor authorisation");
    await client.requestSms({ authyId: req.user.authyID }, {force: true});
    res.send('Sms sent!');
});

router.post('/twofactorauth', auth, async (req,res) => {
    if(!req.user || !req.user.forTwoFactor) return res.status(400).send("Invalid two-factor token");

    const {error} = (Joi.object({
        code: Joi.number().required(),
        save: Joi.bool()
    }).validate(req.body));

    if(error) return res.status(400).send('Validation error: ' + error.details[0].message);

    try {
        const { success } = await client.verifyToken({ authyId: req.user.authyID, token: req.body.code });
        if(success){
            const user = await User.findById(req.user._id);
            const obj = {
                jwt: user.generateAuthToken(true),
                auth: true
            };
            if(req.body.save){
                const sign = config.get('SP_twoFactorKey');
                const trusted = jwt.sign({
                    _id: user._id,
                    email: user.email,
                    signedOn: new Date()
                }, sign);
                obj.trusted = trusted;
            }
            obj.user = filterUser(user);
            return res.json(obj);
        }
        else {
            throw new Error({message: "Invalid token"});
        }
    }
    catch(_){
        return res.status(400).send('Invalid token, please try again');
    }
});

function validate(req) {
    const schema = Joi.object({
      email: Joi.string().min(5).max(255).required().email(),
      password: Joi.string().min(5).max(255).required()
    });
  
    return schema.validate(req);
}

module.exports = router;
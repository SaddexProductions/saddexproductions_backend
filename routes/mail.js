//npm modules
const express = require('express');
const axios = require('axios');
const nodemailer = require('nodemailer');
const Joi = require('@hapi/joi');
const cors = require('cors');
const config = require('config');

//local modules
const { development } = require('../utility/non-env-constants');

//setup
const router = express.Router();
if (development) router.use(cors());

//without this route, the options' request didn't work
router.options('/',(_req,res) => {
    return res.status(200).send("hello");
});

router.post('/', async (req,res) => {

    const schema = Joi.object({
        email: Joi.string().min(10).max(100).email().required(),
        subject: Joi.string().min(3).max(120).required(),
        content: Joi.string().min(5).max(10000).required(),
        captcha: Joi.string().required()
    });

    const {error} = schema.validate(req.body);

    if(error) return res.status(400).send("The field " + error.details[0].message);

    const secretKey = config.get('SP_captchaSecretKey');

    const verifyURL = `https://google.com/recaptcha/api/siteverify?secret=${secretKey}&response=${req.body.captcha}&remoteip=${req.header("X-Real-IP")}`;

    try {
        const aResponse = await axios.get(verifyURL);

        if(aResponse.data.success){
            let transporter = nodemailer.createTransport({
                host: 'smtp.sendgrid.net',
                port: 465,
                secure: true, // true for 465, false for other ports
                auth: {
                    user: "apikey",
                    pass: config.get('nodemailer_key')
                }
            });

            let mailOptions = {
                from: req.body.email, // sender address
                to: 'saddexproductions@gmail.com', // list of receivers
                subject: req.body.subject, // Subject line
                text: req.body.content, // plain text body
                html: `${req.body.content}` // html body
            };

            transporter.sendMail(mailOptions, (error, info) => {
                if (error) {
                    return res.status(500).send("Internal Server Error");
                }
                else {
                    return res.status(200).send("Message successfully sent");
                }
            });
            
        }
        else {
            return res.status(500).send("Captcha verification failed");
        }
    }
    catch(ex){
        return res.status(500).send("Captcha verification failed");
    }
    
});

module.exports = router;
//npm modules
const express = require('express');
const cors = require('cors');

//local modules
const auth = require('../middleware/auth'); //to be implemented in the future
const { Software, validate } = require('../models/software');
const { development } = require('../utility/non-env-constants');

//setup
const router = express.Router();
if (development) router.use(cors());

router.get('/', async (_req, res) => {
    const results = await Software.find({}, { __v: 0 });
    if (!results || results.length == 0) return res.status(404).send("No software found");

    res.status(200).json(results);
});

module.exports = router;
//npm modules
const express = require('express');
const cors = require('cors');

//local modules
const auth = require('../middleware/auth'); // to be implemented in the future
const { Video, validate } = require('../models/video');
const { development } = require('../utility/non-env-constants');

//setup
const router = express.Router();
if (development) router.use(cors());

router.get('/', async (req, res) => {
    let filter = {};
    let order = -1;
    let sortBy = {};
    
    if (req.query.name) filter.name = {$regex: new RegExp(req.query.name, "i")};
    if(req.query.order){
        if(req.query.order == "descending"){
            order = -1;
        }
        else {
            order = 1;
        }
    }
    if(req.query.sound == 'false') filter.sound = false;
    if(req.query.sortBy){
        sortBy[req.query.sortBy] = order;
    }
    else {
        sortBy = {
            uploaded: order
        }
    }
    if(req.query.desc) filter.desc = {$regex: new RegExp(req.query.desc, "i")};

    if(req.query.startDate || req.query.endDate){

        if(req.query.startDate && req.query.endDate && req.query.startDate > req.query.endDate){
            return res.status(401).send("The startdate is more recent than the end date");
        }

        filter.uploaded = {
            $gte: req.query.startDate || new Date(2015,01,01), $lte: req.query.endDate || Date.now()
        };
    }

    const results = await Video.find(filter, { __v: 0 }).sort(sortBy).limit(0);
    if (!results || results.length == 0) return res.send("No results");

    res.status(200).json(results);
});

module.exports = router;
const jwt = require('jsonwebtoken');
const config = require('config');
const { User } = require('../models/user');

module.exports = async function (req, res, next) {
    const token = req.header('x-auth-token');
    if (!token) return res.status(401).send("Access denied. No token provided.");

    try {
        const decoded = jwt.verify(token, config.get('SP_jwtPrivateKey'));
        if (!decoded.login) throw new Error("Token used for password resets can't be used for login");
        req.user = decoded;
        const user = await User.findOne({email: decoded.email});
        if(user.passwordUpdatedOn && Date.parse(user.passwordUpdatedOn) > Date.parse(decoded.signedOn) || !decoded.signedOn)
        return res.status(400).send("This token has expired.");
        next();
    }
    catch (ex) {
        return res.status(400).send("Invalid token");
    }
}
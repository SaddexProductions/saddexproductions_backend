const jwt = require('jsonwebtoken');
const config = require('config');

module.exports = function (req, _, next) {
    const token = req.header('x-trusted');
    req.bypass = false;
    if(!token) return next();

    try {
        const decoded = jwt.verify(token, config.get('SP_twoFactorKey'));
        if(decoded.email !== req.body.email) throw new Error();
        req.bypass = true;
        next();
    }
    catch(ex){
        next();
    }
}
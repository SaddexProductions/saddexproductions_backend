//npm modules
const mongoose = require('mongoose');
const Joi = require('@hapi/joi')
.extend(require('@hapi/joi-date'));

const ProjectSchema = new mongoose.Schema({
    image: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 300
    },
    url: {
        type: String,
        required: true,
        minlength: 15,
        maxlength: 1000
    },
    title: {
        type: String,
        minlength: 5,
        maxlength: 100,
        required: true
    },
    type: {
        type: String,
        minlength: 5,
        maxlength: 200,
        required: true
    },
    frontEnd: {
        type: String,
        minlength: 5,
        maxlength: 100,
        required: true
    },
    backEnd: {
        type: String,
        minlength: 5,
        maxlength: 100,
        required: true
    },
    yMoC: {
        time: {
            type: Date,
            required: true
        },
        workInProgress: Boolean
    }
}, { collection: 'projects' });

const Project = mongoose.model('Projects', ProjectSchema);

function validate(project) {
  const schema = Joi.object({
    image: Joi.string().min(5).max(300).required().regex(/^.*(jpg|JPG|gif|GIF|png|PNG|JPEG|jpeg)$/),
    url: Joi.string().min(15).max(1000).required().regex(/https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/),
    title: Joi.string().min(5).max(100).required(),
    type: Joi.string().min(5).max(200).required(),
    frontEnd: Joi.string().min(5).max(100).required(),
    backEnd: Joi.string().min(5).max(100).required(),
    yMoC: Joi.object({
        time: Joi.date().required(),
        workInProgress: Joi.boolean()
    })
  });

  return schema.validate(project);
}
exports.Project = Project;
exports.validate = validate;
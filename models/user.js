//npm modules
const config = require('config');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const JoiPhone = require('joi-phone-number');
const Joi = require('@hapi/joi').extend(JoiPhone);

const userSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 255,
    unique: true
  },
  password: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 1024,
  },
  twoFactorAuth: {
    cellphone: {
      type: String,
      minlength: 7,
      required: true,
      maxlength: 15,
      validate: /^([0-9][0-9][0-9])\W*([0-9][0-9]{2})\W*([0-9]{0,5})$/
    },
    countryCode: {
      type: String,
      minlength: 2,
      maxlength: 2,
      required: true
    },
    authyID: {
      type: Number,
      required: true
    }
  },
  validResetToken: String,
  passwordUpdatedOn: Date,
  isAdmin: Boolean
}, { collection: 'users' });

userSchema.methods.generateAuthToken = function (login = false, twoFactor = false) {
  const token = jwt.sign({
    _id: this._id,
    isAdmin: this.isAdmin,
    email: this.email,
    login,
    forTwoFactor: twoFactor,
    authyID: twoFactor ? this.twoFactorAuth.authyID : false,
    signedOn: new Date()
  }, config.get("SP_jwtPrivateKey"));
  return token;
}

const User = mongoose.model('User', userSchema);

function validateUser(user) {
  const schema = Joi.object({
    email: Joi.string().min(5).max(255).required().email(),
    password: Joi.string().min(5).max(25).required(),
    repeatPassword: Joi.string().min(5).max(25).required(),
    cellphone: Joi.string().phoneNumber().required(),
    countryCode: Joi.string().required()
  });

  return schema.validate(user);
}
function validateNewPass(req) {
  const schema = Joi.object({
    password: Joi.string().min(5).max(25).required(),
    repeatPassword: Joi.string().min(5).max(25).required()
  });

  return schema.validate(req);
}
function validateEmail(req) {
  const schema = Joi.object({
    email: Joi.string().min(5).max(255).required().email()
  });
  return schema.validate(req);
}
exports.User = User;
exports.validate = validateUser;
exports.validateNewPass = validateNewPass;
exports.validateEmail = validateEmail;
//npm modules
const mongoose = require('mongoose');
const Joi = require('@hapi/joi')
.extend(require('@hapi/joi-date'));

const SoftwareSchema = new mongoose.Schema({
    title: {
      type: String,
      minlength: 5,
      maxlength: 50,
      required: true
    },
    type: {
      type: String,
      minlength: 5,
      maxlength: 10,
      required: true
    },
    url: {
      type: String,
      minlength: 5,
      maxlength: 300,
      required: true
    },
    img: {
      type: String,
      minlength: 2,
      maxlength: 400,
      required: true
    }
}, { collection: 'software' });

const Software = mongoose.model('Software', SoftwareSchema);

function validate(software) {
  const schema = Joi.object({
    title: Joi.string().min(5).max(50).required(),
    type: Joi.string().min(5).max(10).required(),
    url: Joi.string().min(5).max(300).required(),
    img: Joi.string().min(2).max(400).required()
  });

  return schema.validate(software);
}
exports.Software = Software;
exports.validate = validate;
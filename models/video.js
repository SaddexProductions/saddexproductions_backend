//npm modules
const mongoose = require('mongoose');
const Joi = require('@hapi/joi')
.extend(require('@hapi/joi-date'));

const VideosSchema = new mongoose.Schema({
  thumb: {
    type: String,
    required: true,
    unique: true,
    minlength: 5
  },
  name: {
    type: String,
    required: true,
    minlength: 10,
    maxlength: 100
  },
  adress: {
    type: String,
    minlength: 5,
    maxlength: 20,
    required: true
  },
  uploaded: {
    type: Date,
    required: true
  },
  sound: Boolean,
  desc: {
    type: String,
    minlength: 20,
    maxlength: 1000,
    required: true
  }
}, { collection: 'videos' });

const Video = mongoose.model('Videos', VideosSchema);

function validate(video) {
  const schema = Joi.object({
    thumb: Joi.string().min(5).required().regex(/^.*(jpg|JPG|gif|GIF|png|PNG|JPEG|jpeg)$/),
    name: Joi.string().min(10).max(100).required(),
    adress: Joi.string().required().min(5).max(20),
    uploaded: Joi.date().format('YYYY-MM-DD').utc().required(),
    sound: Joi.boolean(),
    desc: Joi.string().required()
  });

  return schema.validate(video);
}
exports.Video = Video;
exports.validate = validate;
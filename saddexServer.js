//npm modules
const config = require('config');
const express = require('express');

//local modules
const env = require('./config/default.json');

//setup
const app = express();

for(let key in env) {
  if(!config.get(key)) throw new Error('Fatal error - ' + key + ' is not defined - check env variables');
}

//startup
require('./startup/logging')();
require('express-async-errors');
require('./startup/routes')(app);
require('./startup/prod')(app);
require('./startup/db')();

app.use(function (error, req, res, next) {
    if (error instanceof SyntaxError) {
      return res.status(400).send("Syntax error");
    } else {
      next();
    }
});

const port = config.get("SP_port");

console.log("Application listening on port " + port);

app.listen(port);
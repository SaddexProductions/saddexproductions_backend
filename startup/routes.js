//npm modules
const bodyParser = require('body-parser');

//local modules
const auth = require('../routes/auth');
const users = require('../routes/users');
const projects = require('../routes/projects');
const mail = require('../routes/mail');
const videos = require('../routes/videos');
const software = require('../routes/software');
const error = require('../middleware/errors');

module.exports = function(app){

    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json());
    app.use('/api/login', auth);
    app.use('/api/users', users);
    app.use('/api/projects', projects);
    app.use('/api/send', mail);
    app.use('/api/software', software);
    app.use('/api/videos', videos);
    app.use(error);
}
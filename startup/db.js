//npm modules
const config = require('config');
const mongoose = require('mongoose');

module.exports = () => {
    mongoose.connect('mongodb://localhost/SaddexProductions', {useNewUrlParser: true, useUnifiedTopology: true,
    useFindAndModify: true, useCreateIndex: true, useFindAndModify: false
});
}


//function to remove initial zero from cellphone number
const removeZero = cellphone => {
    cellphone = cellphone.split('');
    if (cellphone[0] === "0") {
        cellphone.splice(0, 1)
    }
    return cellphone.join('');
}

module.exports = removeZero;
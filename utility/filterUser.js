//npm modules
const _ = require('lodash');

const filterUser = function (user) {
    const obj = _.pick(user, ['_id', 'email']);

    obj.twoFactorAuth = _.pick(user.twoFactorAuth, ['cellphone', 'countryCode']);

    return obj;
}

module.exports = filterUser;